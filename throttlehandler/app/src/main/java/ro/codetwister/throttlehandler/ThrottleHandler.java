package ro.codetwister.throttlehandler;

import android.os.Handler;
import android.os.Looper;

/**
 */
public class ThrottleHandler {

    private Handler handler = new Handler(Looper.getMainLooper());
    private Callback callback;
    private long throttleDelay;
    private long throttleTimeout;
    private long lastThrottleExecution;

    private Runnable throttleRunnable = new Runnable() {
        @Override
        public void run() {
            callback.executeThrottledAction();
            lastThrottleExecution = System.currentTimeMillis();
        }
    };

    public ThrottleHandler(Callback callback, long throttleDelay, long throttleTimeout) {
        this.callback = callback;
        this.throttleDelay = throttleDelay;
        this.throttleTimeout = throttleTimeout;
    }

    public void throttle() {
        if (lastThrottleExecution == 0) {
            lastThrottleExecution = System.currentTimeMillis();
        }
        handler.removeCallbacks(throttleRunnable);
        if (System.currentTimeMillis() - throttleTimeout > lastThrottleExecution) {
            handler.post(throttleRunnable);
        } else {
            handler.postDelayed(throttleRunnable, throttleDelay);
        }
    }

    public interface Callback {
        void executeThrottledAction();
    }
}
